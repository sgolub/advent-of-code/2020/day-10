package main

import (
	"fmt"
	"sort"

	"gitlab.com/sgolub/advent-of-code/base/v2/days"
)

const dataKey = "adapters"

type myDay struct {
	data map[string]string
}

func (d myDay) SetData(key, value string) {
	d.data[key] = value
}

// Year will return the AOC Year
func (d myDay) Year() int {
	return 2020
}

// Day will return the day number for this puzzle
func (d myDay) Day() int {
	return 10
}

// GetData will load the data and parse it from disk
func (d myDay) GetData(useSampleData bool) []int {
	result := make([]int, 0)
	data, ok := days.Control().LoadData(d, useSampleData)[dataKey]
	if !ok {
		return result
	}
	for _, i := range data.([]interface{}) {
		result = append(result, i.(int))
	}
	sort.Ints(result)
	// Adding in the outlet at position zero and appending the handheld at the end
	result = append(result, result[len(result)-1]+3)
	result = append([]int{0}, result...)
	return result
}

// Solution1 is the solution to the first part of the puzzle
func (d myDay) Solution1(useSample bool) string {
	data := d.GetData(useSample)
	defer days.NewTimer("D10P1")
	count1 := 0
	count3 := 0
	for i := -1; i < len(data)-1; i++ {
		num := 0
		if i > -1 {
			num = data[i]
		}
		switch data[i+1] - num {
		case 1:
			count1++
		case 3:
			count3++
		}
	}
	return fmt.Sprint(count1 * count3)
}

// Solution2 is the solution to the second part of the puzzle
func (d myDay) Solution2(useSample bool) string {
	data := d.GetData(useSample)
	defer days.NewTimer("D10P2")

	n := len(data)
	dp := make([]int, n)
	dp[0] = 1

	for i := 1; i < n; i++ {
		for j := 0; j < i; j++ {
			if data[i]-data[j] <= 3 {
				dp[i] += dp[j]
			}
		}
	}
	return fmt.Sprint(dp[n-1])
}

// GetDay will return this module's day. Used by the plugin loader.
func GetDay() days.Day {
	return myDay{
		data: map[string]string{},
	}
}
